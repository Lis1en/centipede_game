using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class MushroomController : MonoBehaviour
{
    [SerializeField] private Transform playAreaContainer;
    [SerializeField] private int mushroomHP;
    [SerializeField] private float mushroomSpawnPercentPerCell;
    [SerializeField] private int minSpawnY;
    private List<MushroomObject> _currentMushroom = new List<MushroomObject>();

    public void StartGame()
    {
        SpawnMushroom();
    }

    private void SpawnMushroom()
    {
        var playArea = GameController.Instance.PlayAreaSize;
        var maxSpawnY = playArea.y - 1;
        for (int x = 0; x < playArea.x - 1; x++)
        {
            for (int y = minSpawnY; y < playArea.y; y++)
            {
                if (y >= maxSpawnY) continue;

                var targetBlock = GameController.Instance.GetBoardTarget(x, y);
                if (targetBlock != null) continue;

                bool isSpawn = Random.Range(0f, 1f) <= mushroomSpawnPercentPerCell;
                if (!isSpawn) continue;

                var mushroomObject = MushroomObjectPool.Instance.GetObject();
                mushroomObject.transform.parent = playAreaContainer;

                mushroomObject.SetConfig(this, mushroomHP);
                mushroomObject.SetPosition(new Vector2(x, y));
                GameController.Instance.RegisterObject(mushroomObject, mushroomObject.CurrentPosition);

                _currentMushroom.Add(mushroomObject);
            }
        }
    }

    public void ResetGame()
    {
        var currentMushroom = new List<MushroomObject>(_currentMushroom);
        currentMushroom.ForEach(mushroomObject =>
        {
            if (mushroomObject == null) return;
            mushroomObject.DeSpawn();
        });
        _currentMushroom.Clear();
    }

    public void RemoveMushroom(MushroomObject mushroomObject)
    {
        _currentMushroom.Remove(mushroomObject);
    }
}
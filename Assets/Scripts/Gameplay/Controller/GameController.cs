using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Mathematics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class GameController : Singleton<GameController>
{
    public GameState CurrentState => currentState;

    [SerializeField] private CameraController cameraController;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private BulletController bulletController;
    [SerializeField] private CentipedeController centipedeController;
    [SerializeField] private MushroomController mushroomController;
    [SerializeField] private SpiderController spiderController;

    [SerializeField] private Vector2 playAreaSize;
    [SerializeField] private int maxPlayerLife;
    [SerializeField] private int scorePerCentipedePart;
    [SerializeField] private int scorePerMushroom;
    [SerializeField] private int scorePerSpider;
    [SerializeField] private GameState currentState;

    private BlockObject[,] _gameBoard;
    private int _score;
    private int _playerLife;

    public Vector2 PlayAreaSize => playAreaSize;
    public Vector2 PlayerPos => playerController.PlayerObject.CurrentPosition;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        _gameBoard = new BlockObject[(int) playAreaSize.x, (int) playAreaSize.y];
        PathFinderController.Init();
        cameraController.Init(playAreaSize);
        
    }

    public void StartGame()
    {
        Debug.Assert(currentState == GameState.Start);

        _score = 0;
        _playerLife = maxPlayerLife;

        UIController.Instance.StartGame();
        UIController.Instance.UpdatePlayerLife(_playerLife);
        UIController.Instance.UpdateScore(_score);
        
        playerController.StartGame();
        centipedeController.StartGame();
        mushroomController.StartGame();
        spiderController.StartGame();

        currentState = GameState.Playing;
    }

    public void ReStartGame()
    {
        playerController.ResetGame();
        centipedeController.ResetGame();
        mushroomController.ResetGame();
        bulletController.ResetGame();
        spiderController.ResetGame();

        currentState = GameState.Start;
        UIController.Instance.ReStartGame();
    }

    private void ResetGame()
    {
        currentState = GameState.Reset;
        
        _gameBoard = new BlockObject[(int) playAreaSize.x, (int) playAreaSize.y];
        
        playerController.ResetGame();
        centipedeController.ResetGame();
        mushroomController.ResetGame();
        bulletController.ResetGame();
        spiderController.ResetGame();

        playerController.StartGame();
        centipedeController.StartGame();
        mushroomController.StartGame();
        currentState = GameState.Playing;
    }

    public void DestroyCentipedePart()
    {
        _score += scorePerCentipedePart;
        UIController.Instance.UpdateScore(_score);
    }

    public void DestroyMushroom()
    {
        _score += scorePerMushroom;
        UIController.Instance.UpdateScore(_score);
    }

    public void DestroySpider()
    {
        _score += scorePerSpider;
        UIController.Instance.UpdateScore(_score);
    }
    
    public void PlayerHit()
    {
        _playerLife -= 1;
        _score = 0;
        UIController.Instance.UpdatePlayerLife(_playerLife);
        UIController.Instance.UpdateScore(_score);


        if (_playerLife > 0)
        {
            ResetGame();
            return;
        }
        
        currentState = GameState.Over;
        UIController.Instance.GameOver(_score);
    }

    public void PlayerKill()
    {
        _playerLife = 0;
        PlayerHit();
    }

    public void GameFinish()
    {
        currentState = GameState.Over;
        UIController.Instance.GameOver(_score);
    }

    #region GameBoard

    public BlockObject GetBoardTarget(int x, int y)
    {
        Debug.Assert(x >= 0 && x < _gameBoard.GetLength(0) && y >= 0 && y < _gameBoard.GetLength(1), $"x: {x} | y: {y}");
        return _gameBoard[x, y];
    }

    public void RegisterObject(BlockObject blockObject, Vector2 position)
    {
        Debug.Assert(position.x <= _gameBoard.GetLength(0) - 1);
        Debug.Assert(position.y <= _gameBoard.GetLength(1) - 1);
        Debug.Assert(_gameBoard[(int) position.x, (int) position.y] == null);

        var xPos = (int) position.x;
        var yPos = (int) position.y;
        _gameBoard[xPos, yPos] = blockObject;
    }

    public void MoveObject(BlockObject moveObject, Vector2 moveVector)
    {
        int toX = Mathf.RoundToInt(moveObject.CurrentPosition.x + moveVector.x);
        int toY = Mathf.RoundToInt(moveObject.CurrentPosition.y + moveVector.y);
        var toPos = new Vector2(toX, toY);

        if (_gameBoard[toX, toY] != null)
        {
            var blockObject = _gameBoard[toX, toY];
            Collide(moveObject, blockObject);
            return;
        }

        _gameBoard[toX, toY] = moveObject;
        _gameBoard[(int) moveObject.CurrentPosition.x, (int) moveObject.CurrentPosition.y] = null;
        moveObject.SetPosition(toPos);
    }

    public void PlaceObject(BlockObject moveObject, Vector2 targetPos)
    {
        Debug.Assert(targetPos.x >= 0 && targetPos.x < _gameBoard.GetLength(0));
        Debug.Assert(targetPos.y >= 0 && targetPos.y < _gameBoard.GetLength(1));

        var targetObject = _gameBoard[(int) targetPos.x, (int) targetPos.y];
        if (targetObject != null)
        {
            Collide(moveObject, targetObject);
            return;
        }

        _gameBoard[(int) moveObject.CurrentPosition.x, (int) moveObject.CurrentPosition.y] = null;
        _gameBoard[(int) targetPos.x, (int) targetPos.y] = moveObject;
        moveObject.SetPosition(targetPos);
    }

    private void Collide(BlockObject moveObject, BlockObject blockObject)
    {
        moveObject.Collide(blockObject);
    }

    public void RemoveObject(BlockObject removeObject)
    {
        _gameBoard[(int) removeObject.CurrentPosition.x, (int) removeObject.CurrentPosition.y] = null;
    }
    #endregion
}
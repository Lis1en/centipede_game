using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class CentipedeController : Singleton<CentipedeController>
{
    [SerializeField] private Transform centipedeContainer;
    [SerializeField] private int centipedeSize;
    [SerializeField] private float speedCellPerSec;

    private List<CentipedeObject> _currentCentipede = new List<CentipedeObject>();

    public void StartGame()
    {
        SpawnCentipede();
    }

    public void ResetGame()
    {
        var currentCentipede = new List<CentipedeObject>(_currentCentipede);
        currentCentipede.ForEach(centipede =>
        {
            if (centipede == null) return;
            centipede.DeSpawn();
        });
        _currentCentipede.Clear();
    }

    private void SpawnCentipede()
    {
        var centipedeObject = CentipedeObjectPool.Instance.GetObject();
        centipedeObject.transform.SetParent(centipedeContainer);

        var centipedeHeadObject = CentipedeHeadObjectPool.Instance.GetObject();
        var centipedeBodyObjects = new List<CentipedeBodyObject>();

        centipedeHeadObject.transform.SetParent(centipedeObject.transform);

        for (int i = 0; i < centipedeSize - 1; i++)
        {
            var bodyObject = CentipedeBodyObjectPool.Instance.GetObject();
            bodyObject.transform.SetParent(centipedeObject.transform);
            centipedeBodyObjects.Add(bodyObject);
        }

        centipedeObject.Spawn(
            this,
            centipedeHeadObject,
            centipedeBodyObjects,
            Vector2.right,
            CentipedeObject.MoveState.TopToBottom,
            speedCellPerSec);

        centipedeObject.SetStartGamePosition();

        AddCentipede(centipedeObject);
    }

    public void AddCentipede(CentipedeObject centipedeObject)
    {
        _currentCentipede.Add(centipedeObject);
    }

    public void RemoveCentipede(CentipedeObject centipedeObject)
    {
        _currentCentipede.Remove(centipedeObject);
        
        if (GameController.Instance.CurrentState != GameState.Reset && _currentCentipede.Count == 0) GameController.Instance.GameFinish();
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpiderController : MonoBehaviour
{
    [SerializeField] private Transform playAreaContainer;
    [SerializeField] private float spawnIntervalSec;
    [SerializeField] private float speedCellPerSec;
    [SerializeField] private float spiderHoldTime;
    [SerializeField] private Vector2 spawnAreaRange;

    private Vector2 _spawnAreaX;
    private Vector2 _spawnAreaY;

    private List<SpiderObject> _currentSpider = new List<SpiderObject>();
    private float _spawnCountDownTime;
    private bool _isSpiderActive;

    public void StartGame()
    {
        var playAreaSize = GameController.Instance.PlayAreaSize;
        var centerX = Mathf.RoundToInt(playAreaSize.x / 2);
        var centerY = Mathf.RoundToInt(playAreaSize.y / 2);
        _spawnAreaX = new Vector2(centerX - spawnAreaRange.x / 2, centerX + spawnAreaRange.x / 2);
        _spawnAreaY = new Vector2(centerY - spawnAreaRange.y / 2, centerY + spawnAreaRange.y / 2);
    }

    private void Update()
    {
        if (GameController.Instance.CurrentState == GameState.Playing)
        {
            SpawnInterval();
        }
    }

    private void SpawnInterval()
    {
        if (_isSpiderActive) return;
        if (_spawnCountDownTime < spawnIntervalSec)
        {
            _spawnCountDownTime += Time.deltaTime;
            return;
        }

        List<Vector2> spawnPositionPool = GetSpawnPositionPool();
        if (spawnPositionPool.Count <= 0) return;
        var spawnPosition = spawnPositionPool[Random.Range(0, spawnPositionPool.Count)];

        var spiderObject = SpiderObjectPool.Instance.GetObject();
        _currentSpider.Add(spiderObject);
        
        spiderObject.transform.SetParent(playAreaContainer);
        spiderObject.SetPosition(spawnPosition);
        spiderObject.SetConfig(this, speedCellPerSec, spiderHoldTime);
        GameController.Instance.RegisterObject(spiderObject, spiderObject.CurrentPosition);
        
        _isSpiderActive = true;
        _spawnCountDownTime = 0;
    }

    private List<Vector2> GetSpawnPositionPool()
    {
        var spawnPositionPool = new List<Vector2>();
        for (int x = (int) _spawnAreaX.x; x < _spawnAreaX.y; x++)
        {
            for (int y = (int) _spawnAreaY.x; y < _spawnAreaY.y; y++)
            {
                if (GameController.Instance.GetBoardTarget(x, y) != null) continue;
                spawnPositionPool.Add(new Vector2(x, y));
            }
        }

        return spawnPositionPool;
    }

    public void ResetGame()
    {
        _spawnCountDownTime = 0f;
        
        var currentSpider = new List<SpiderObject>(_currentSpider);
        currentSpider.ForEach(spider =>
        {
            if (spider == null) return;
            spider.DeSpawn();
        });
        _currentSpider.Clear();
    }

    public void RemoveSpider(SpiderObject spiderObject)
    {
        _currentSpider.Remove(spiderObject);
        _isSpiderActive = _currentSpider.Count > 0;
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraController : Singleton<CameraController>
{
    public void Init(Vector2 playAreaSize)
    {
        var cam = GetComponent<Camera>();
        playAreaSize /=  2;
        playAreaSize += new Vector2(0, 1);
        var size = Math.Max(playAreaSize.x, playAreaSize.y);
        cam.orthographicSize = size;
        cam.transform.position = new Vector3(playAreaSize.x, playAreaSize.y, -10);
    }
}

using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using Unity.Mathematics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerObject playerObject;

    [SerializeField] private float speedCellPerSec;
    [SerializeField] private float playSpaceInPercent;
    [SerializeField] private float invincibleTime;
    [SerializeField] BulletController bulletController;

    public PlayerObject PlayerObject => playerObject;

    private bool _isNewInput = true;
    private bool _isStartGameInput = true;
    private bool _isInvincible;

    public void StartGame()
    {
        SetupPlayer();
    }

    private void SetupPlayer()
    {
        var playAreaSize = GameController.Instance.PlayAreaSize;
        var xPos = Mathf.Round(playAreaSize.x / 2) - 1;
        var targetPos = new Vector2(xPos, 0);

        playerObject.SetPosition(targetPos);
        playerObject.SetupConfig(this, speedCellPerSec, playSpaceInPercent);
        GameController.Instance.RegisterObject(playerObject, targetPos);
    }

    private void Update()
    {
        switch (GameController.Instance.CurrentState)
        {
            case GameState.Playing:
                ReceivedInput();
#if DEBUG
                if (Input.GetKeyDown(KeyCode.E)) PlayerHit();
#endif
                return;
            case GameState.Start:
                if (Input.GetKeyDown(KeyCode.Space)) GameController.Instance.StartGame();
                break;
            case GameState.Over:
                if (Input.GetKeyDown(KeyCode.Space)) GameController.Instance.ReStartGame();
                break;
        }
    }

    public void ResetGame()
    {
        _isStartGameInput = true;
        playerObject.ResetGame();
    }

    public void PlayerHit()
    {
        GameController.Instance.PlayerHit();

        // Invincible Flow
        // Debug.Assert(!_isInvincible, "Player cannot be hit while being invincible");
        // if (_isInvincible) return;
        // SetInvincible(true);
        //
        // playerObject.PlayInvincibleEffect();
        // await Task.Delay(Mathf.RoundToInt(1000 * invincibleTime));
        // playerObject.StopInvincibleEffect();
        // SetInvincible(false);
    }


    private void SetInvincible(bool isInvincible)
    {
        _isInvincible = isInvincible;
        if (isInvincible)
        {
            GameController.Instance.RemoveObject(playerObject);
        }
        else
        {
            GameController.Instance.PlaceObject(playerObject, playerObject.CurrentPosition);
        }
    }

    private void ReceivedInput()
    {
        #region MovementInput

        // var hAxis = Input.GetAxis(Constant.PlayerInput.Horizontal);
        // var vAxis = Input.GetAxis(Constant.PlayerInput.Vertical);

        float hAxis = 0;
        float vAxis = 0;

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) hAxis -= 1;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) hAxis += 1;
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) vAxis += 1;
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) vAxis -= 1;
        
        if (hAxis != 0 || vAxis != 0) playerObject.Move(hAxis, vAxis, _isNewInput, _isInvincible);
        _isNewInput = hAxis == 0 && vAxis == 0;

        #endregion

        #region ShootInput

        if (!_isStartGameInput && Input.GetKey(KeyCode.Space)) bulletController.Shoot(playerObject.CurrentPosition + Vector2.up);
        if (_isStartGameInput && Input.GetKeyUp(KeyCode.Space)) _isStartGameInput = false;

        #endregion
    }
}
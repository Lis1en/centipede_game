using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class BulletController : MonoBehaviour
{
    [SerializeField] private Transform bulletContainer;
    [SerializeField] private int speedCellPerSec;
    [SerializeField] private int bulletPerSec;
    private List<BulletObject> _currentBullet = new List<BulletObject>();

    private float _fireRatePerSec;
    private float _fireRateCountTime;
    private float _lastFireTime;

    private void Awake()
    {
        Debug.Assert(speedCellPerSec > bulletPerSec,
            "speedCellPerSec need to be faster than bulletPerSec otherwise the bullet will collide itself upon spawning the new one");
        
        _fireRatePerSec = 1f / bulletPerSec;
    }

    public void Shoot(Vector2 startPosition)
    {
        if (Time.time > _lastFireTime + _fireRatePerSec) _fireRateCountTime = _fireRatePerSec;
        if (_fireRateCountTime < _fireRatePerSec)
        {
            _fireRateCountTime += Time.deltaTime;
            return;
        }

        _fireRateCountTime = 0f;
        _lastFireTime = Time.time;
        SpawnBullet(startPosition);
    }

    private void SpawnBullet(Vector2 startPosition)
    {
        var bulletObject = BulletObjectPool.Instance.GetObject();
        _currentBullet.Add(bulletObject);
        
        bulletObject.transform.SetParent(bulletContainer);
        bulletObject.Spawn(this, startPosition, speedCellPerSec);
    }

    public void RemoveBullet(BulletObject bulletObject)
    {
        _currentBullet.Remove(bulletObject);
    }

    public void ResetGame()
    {
        var currentBullet = new List<BulletObject>(_currentBullet);
        currentBullet.ForEach(bullet =>
        {
            if (bullet == null) return;
            bullet.DeSpawn();
        });
        _currentBullet.Clear();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomObject : BlockObject
{
    [SerializeField] private SpriteRenderer mushroomGraphic;
    [SerializeField] private List<Sprite> mushroomSprites;
    [SerializeField] private List<Color> mushroomColors;
    private MushroomController _mushroomController;
    
    private int _maxHP;
    private int _mushroomHP;
    
    public void SetConfig(MushroomController mushroomController, int mushroomHP)
    {
        _mushroomController = mushroomController;
        _maxHP = mushroomHP;
        _mushroomHP = mushroomHP;
        
        var spriteIndex = mushroomSprites.Count - 1 ;
        mushroomGraphic.sprite = mushroomSprites[spriteIndex];
        mushroomGraphic.color = mushroomColors[spriteIndex];
    }
    
    public override void DeSpawn()
    {
        _mushroomController.RemoveMushroom(this);
        GameController.Instance.RemoveObject(this);
        MushroomObjectPool.Instance.ReturnObject(transform);
        
        base.DeSpawn();
    }

    public override void Collide(BlockObject targetObject)
    {
        base.Collide(targetObject);
        if (targetObject is BulletObject)
        {
            BulletHit();
        }
    }

    public void BulletHit()
    {
        _mushroomHP--;

        if (_mushroomHP <= 0)
        {
            DeSpawn();
            return;
        }
        
        var spriteIndex = _mushroomHP * _maxHP / mushroomSprites.Count;
        mushroomGraphic.sprite = mushroomSprites[spriteIndex];
        mushroomGraphic.color = mushroomColors[spriteIndex];
    }
}

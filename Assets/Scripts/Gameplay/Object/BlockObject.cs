using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BlockObject : MonoBehaviour
{
    public Vector2 CurrentPosition { get; protected set; }

    public void SetPosition(Vector2 toPos)
    {
        CurrentPosition = toPos;
        transform.localPosition = toPos;
    }

    public virtual void Collide(BlockObject targetObject)
    {
        
    }

    public virtual void DeSpawn()
    {
        CurrentPosition = Vector2.zero;
    }
}
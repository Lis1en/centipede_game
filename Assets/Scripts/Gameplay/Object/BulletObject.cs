using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BulletObject : BlockObject
{
    private BulletController _bulletController;
    private readonly Vector2 _moveDirection = Vector2.up;

    private float _speedCellPerSec;
    private float _currentTravelTime;
    
    public void Spawn(BulletController bulletController, Vector2 startPosition, float speedCellPerSec)
    {
        _bulletController = bulletController;
        _speedCellPerSec = speedCellPerSec;
        GameController.Instance.PlaceObject(this, startPosition);
    }

    public override void DeSpawn()
    {
        _bulletController.RemoveBullet(this);
        BulletObjectPool.Instance.ReturnObject(transform);
        GameController.Instance.RemoveObject(this);
    }

    private void Update()
    {
        if (GameController.Instance.CurrentState == GameState.Playing)
            MoveRoutine();
    }

    private void MoveRoutine()
    {
        if (_currentTravelTime < 1f / _speedCellPerSec)
        {
            _currentTravelTime += Time.deltaTime;
            return;
        }
        
        if (IsBulletOutSideOfGameBoard())
        {
            DeSpawn();
            return;
        }
        
        GameController.Instance.MoveObject(this, _moveDirection);
        _currentTravelTime = 0;
    }

    private bool IsBulletOutSideOfGameBoard()
    {
        var targetPos = CurrentPosition + _moveDirection;
        return targetPos.y >= GameController.Instance.PlayAreaSize.y;
    }

    public override void Collide(BlockObject targetObject)
    {
        base.Collide(targetObject);
        switch (targetObject)
        {
            case BulletObject _:
            {
                DeSpawn();
                return;
            }
            case MushroomObject mushroomObject:
            {
                mushroomObject.BulletHit();
                DeSpawn();
                GameController.Instance.DestroyMushroom();
                return;
            }
            case SpiderObject spiderObject:
            {
                GameController.Instance.DestroySpider();
                spiderObject.DeSpawn();
                DeSpawn();
                return;
            }
            case CentipedeHeadObject centipedeHeadObject:
            {
                var hitIndex = Constant.CentipedePart.HeadIndex;
                var centipedeObject = centipedeHeadObject.BaseCentipede;
                centipedeObject.SplitCentipede(hitIndex);
                DeSpawn();
                return;
            }
            case CentipedeBodyObject centipedeBodyObject:
            {
                GameController.Instance.DestroyCentipedePart();
                var centipedeObject = centipedeBodyObject.BaseCentipede;
                var hitIndex = centipedeObject.BodyObjects.FindIndex(item => item == centipedeBodyObject);
                centipedeObject.SplitCentipede(hitIndex);
                DeSpawn();
                return;
            }
            default:
            {
                DeSpawn();
                return;
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeBodyObject : BlockObject
{
    private CentipedeObject _baseCentipede;
    public CentipedeObject BaseCentipede { get => _baseCentipede; set => _baseCentipede = value;}

    public override void DeSpawn()
    {
        _baseCentipede = null;
        CentipedeBodyObjectPool.Instance.ReturnObject(transform);
        GameController.Instance.RemoveObject(this);
        base.DeSpawn();
    }
    
    public override void Collide(BlockObject targetObject)
    {
        base.Collide(targetObject);

        switch (targetObject)
        {
            case BulletObject _:
                GameController.Instance.DestroyCentipedePart();
                _baseCentipede.SplitCentipede(Constant.CentipedePart.HeadIndex);
                targetObject.DeSpawn();
                break;
            case PlayerObject playerObject:
                playerObject.HitCentipede();
                break;
        }
    }
}

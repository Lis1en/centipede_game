using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class CentipedeObject : MonoBehaviour
{
    public enum MoveState
    {
        TopToBottom,
        BottomToTop
    }

    private CentipedeHeadObject _headObject;
    private List<CentipedeBodyObject> _bodyObjects;

    private MoveState _currentMoveState;
    private Vector2 _currentMoveDirection;
    public Vector2 CurrentMoveDirection => _currentMoveDirection;
    public List<CentipedeBodyObject> BodyObjects => _bodyObjects;

    private CentipedeController _centipedeController;
    private float _currentTravelTime;
    private float _speedCellPerSec;


    public void Spawn(CentipedeController controller, CentipedeHeadObject headObject, List<CentipedeBodyObject> bodyObjects, Vector2 moveDirection, MoveState moveState, float speed)
    {
        _centipedeController = controller;
        _headObject = headObject;
        _bodyObjects = bodyObjects;

        _headObject.BaseCentipede = this;
        _headObject.transform.SetParent(transform);
        _bodyObjects.ForEach(bodyObject =>
        {
            bodyObject.BaseCentipede = this;
            bodyObject.transform.SetParent(transform);
        });

        _currentMoveDirection = moveDirection;
        _currentMoveState = moveState;
        _speedCellPerSec = speed;
    }

    public void DeSpawn()
    {
        DeSpawnHead();
        DeSpawnBody();
        CentipedeObjectPool.Instance.ReturnObject(transform);
        _centipedeController.RemoveCentipede(this);
    }

    private void DeSpawnHead()
    {
        if (_headObject != null) _headObject.DeSpawn();
        _headObject = null;
    }

    private void DeSpawnBody()
    {
        if (_bodyObjects.Count > 0) _bodyObjects.ForEach(body => body.DeSpawn());
        _bodyObjects.Clear();
    }

    public void SetStartGamePosition()
    {
        var playAreaSize = GameController.Instance.PlayAreaSize;
        var xPos = Mathf.Round(playAreaSize.x / 2);
        var yPos = (int) playAreaSize.y - 1;
        var targetPos = new Vector2(xPos, yPos);

        _headObject.SetPosition(targetPos);
        GameController.Instance.RegisterObject(_headObject, targetPos);
        for (int i = 0; i < _bodyObjects.Count; i++)
        {
            var targetBodyPos = new Vector2(xPos - (i + 1), yPos);
            GameController.Instance.RegisterObject(_bodyObjects[i], targetBodyPos);
            _bodyObjects[i].SetPosition(targetBodyPos);
        }
    }

    private void Update()
    {
        if (GameController.Instance.CurrentState == GameState.Playing)
        {
            MoveRoutine();
        }
    }

    #region Move

    private void MoveRoutine()
    {
        if (_currentTravelTime < 1f / _speedCellPerSec)
        {
            _currentTravelTime += Time.deltaTime;
            return;
        }

        Move();

        _currentTravelTime = 0f;
    }

    private void Move()
    {
        var targetPos = _headObject.CurrentPosition + _currentMoveDirection;
        targetPos = CheckBlockAblePath(targetPos);
        RotateHeadGraphic(targetPos);

        var lastPosition = _headObject.CurrentPosition;
        GameController.Instance.PlaceObject(_headObject, targetPos);
        for (var i = 0; i < _bodyObjects.Count; i++)
        {
            var bodyObject = _bodyObjects[i];
            var lastBodyPosition = bodyObject.CurrentPosition;
            GameController.Instance.PlaceObject(bodyObject, lastPosition);
            lastPosition = lastBodyPosition;
        }
    }

    private Vector2 CheckBlockAblePath(Vector2 targetPos)
    {
        var verticalModifier = _currentMoveState == MoveState.TopToBottom ? -1 : 1;

        targetPos = ValidateTargetPosition(targetPos, out var isValidPos);
        if (isValidPos) return targetPos;

        // 0. Reverse direction
        ReverseDirection();
        if (_bodyObjects.Count > 0)
        {
            targetPos.x = _headObject.CurrentPosition.x + _currentMoveDirection.x;
            ValidateTargetPosition(targetPos, out isValidPos);
            if (isValidPos) return targetPos;
        }

        // 1. go to another Y
        targetPos.x = _headObject.CurrentPosition.x;
        targetPos.y += verticalModifier;
        ValidateTargetPosition(targetPos, out isValidPos);
        if (isValidPos) return targetPos;

        // 2. another Y blocked invert Y
        ReverseDirection();
        targetPos.y = _headObject.CurrentPosition.y - verticalModifier;
        ValidateTargetPosition(targetPos, out isValidPos);
        if (isValidPos) return targetPos;

        // 3. invertY block reverse
        ReverseCentipede();
        ReverseDirection();
        targetPos = _headObject.CurrentPosition + _currentMoveDirection;
        ValidateTargetPosition(targetPos, out isValidPos);
        if (isValidPos) return targetPos;
    
        // 4. Stop movement
        targetPos = _headObject.CurrentPosition;

        return targetPos;
    }

    private Vector2 ValidateTargetPosition(Vector2 targetPos, out bool isValid)
    {
        var isBorder = IsBorder(targetPos);
        var targetBlock = isBorder ? null : GameController.Instance.GetBoardTarget((int) targetPos.x, (int) targetPos.y);
        var isHitAbleBlock = targetBlock == null || IsBlockByHitAbleObject(targetBlock);
        isValid = !isBorder && isHitAbleBlock;

        return targetPos;
    }

    private void ReverseDirection()
    {
        _currentMoveDirection = _currentMoveDirection == Vector2.left ? Vector2.right : Vector2.left;
    }

    private void ReverseCentipede()
    {
        if (_bodyObjects.Count <= 0) return;
        
        var lastBody = _bodyObjects.Last();
        _bodyObjects.RemoveAt(_bodyObjects.Count - 1);
        _bodyObjects.Reverse();
        _bodyObjects.Add(lastBody);
        var tempHeadPos = _headObject.CurrentPosition;
        GameController.Instance.RemoveObject(lastBody);
        GameController.Instance.PlaceObject(_headObject, lastBody.CurrentPosition);
        GameController.Instance.PlaceObject(lastBody, tempHeadPos);
    }

    private bool IsBlockByHitAbleObject(BlockObject targetBlock)
    {
        return !(targetBlock is MushroomObject) &&
               !(targetBlock is CentipedeHeadObject) &&
               !(targetBlock is CentipedeBodyObject);
    }

    private bool IsBorder(Vector2 targetPos)
    {
        var playArea = GameController.Instance.PlayAreaSize;

        if (targetPos.y < 0) _currentMoveState = MoveState.BottomToTop;
        else if (targetPos.y >= playArea.y) _currentMoveState = MoveState.TopToBottom;

        return targetPos.x < 0 || targetPos.x >= playArea.x || targetPos.y < 0 || targetPos.y >= playArea.y;
    }

    private void RotateHeadGraphic(Vector2 targetPos)
    {
        var currentDirection = targetPos - _headObject.CurrentPosition;
        var direction = TypeConvertor.Vector2ToDirection(currentDirection);
        _headObject.SetHeadRotationGraphic(direction);
    }
    #endregion

    #region Split

    public void SplitCentipede(int splitIndex)
    {
        if (_bodyObjects.Count == 0)
        {
            DeSpawnHead();
            DeSpawn();
            return;
        }

        CentipedeObject hitNode;
        // Split Head
        bool isSplitHead = splitIndex == Constant.CentipedePart.HeadIndex;
        if (isSplitHead)
        {
            // splitIndex = -1;
            hitNode = _headObject.BaseCentipede;
        }
        else
        {
            hitNode = _bodyObjects[splitIndex].BaseCentipede;
            _bodyObjects[splitIndex].DeSpawn();
            if (splitIndex == _bodyObjects.Count - 1)
            {
                _bodyObjects.RemoveAt(splitIndex);
                return;
            }
        }

        var newCentipedeObject = CentipedeObjectPool.Instance.GetObject();
        newCentipedeObject.transform.SetParent(transform.parent);

        var transferCentipedeBodys = new List<CentipedeBodyObject>();
        for (int i = _bodyObjects.Count - 1; i > splitIndex; i--)
        {
            var transferBodyObject = _bodyObjects[i];
            transferBodyObject.transform.SetParent(newCentipedeObject.transform);
            transferCentipedeBodys.Add(transferBodyObject);
        }

        if (isSplitHead) splitIndex = 0;
        int totalRemove = _bodyObjects.Count - splitIndex;
        _bodyObjects.RemoveRange(splitIndex, totalRemove);
        
        var newHeadCentipede = CentipedeHeadObjectPool.Instance.GetObject();
        newHeadCentipede.transform.SetParent(newCentipedeObject.transform);
        newHeadCentipede.SetPosition(transferCentipedeBodys.First().CurrentPosition);
        transferCentipedeBodys.First().DeSpawn();
        transferCentipedeBodys.RemoveAt(0);

        var direction = hitNode.CurrentMoveDirection == Vector2.left ? Vector2.right : Vector2.left;
        newCentipedeObject.Spawn(
            _centipedeController,
            newHeadCentipede,
            transferCentipedeBodys,
            direction,
            _currentMoveState,
            _speedCellPerSec);

        _centipedeController.AddCentipede(newCentipedeObject);

        if (isSplitHead) DeSpawn();
    }

    #endregion
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeHeadObject : BlockObject
{
    [SerializeField] private SpriteRenderer graphic;
    private CentipedeObject _baseCentipede;
    public CentipedeObject BaseCentipede { get => _baseCentipede; set => _baseCentipede = value;}

    public override void DeSpawn()
    {
        _baseCentipede = null;
        CentipedeHeadObjectPool.Instance.ReturnObject(transform);
        GameController.Instance.RemoveObject(this);
        base.DeSpawn();
    }

    public override void Collide(BlockObject targetObject)
    {
        base.Collide(targetObject);

        switch (targetObject)
        {
            case BulletObject bulletObject:
                GameController.Instance.DestroyCentipedePart();
                _baseCentipede.SplitCentipede(Constant.CentipedePart.HeadIndex);
                bulletObject.DeSpawn();
                break;
            case PlayerObject playerObject:
                playerObject.HitCentipede();
                break;
        }
    }

    public void SetHeadRotationGraphic(Direction direction)
    {
        if (direction == Direction.None) return;
        graphic.transform.localEulerAngles = new Vector3(0, 0, -(90 * (int) direction));
    }
}

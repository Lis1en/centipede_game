using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerObject : BlockObject
{
    [SerializeField] private SpriteRenderer playerGraphic;
    [SerializeField] private bool isLimitPlayArea;

    private PlayerController _PlayerController;
    private float _speedCellPerSec;
    private float _playSpaceInPercent;

    private float _currentTravelTime;
    private bool _isInvincible;

    public void SetupConfig(PlayerController playerController, float speedCellPerSec, float playSpaceInPercent)
    {
        playerGraphic.gameObject.SetActive(true);
        _PlayerController = playerController;
        _speedCellPerSec = speedCellPerSec;
        _playSpaceInPercent = playSpaceInPercent;
    }

    public void ResetGame()
    {
        playerGraphic.gameObject.SetActive(false);
        GameController.Instance.RemoveObject(this);
    }

    public void Move(float hAxis, float vAxis, bool isNewInput, bool isInvincible)
    {
        if (!isNewInput && _currentTravelTime < 1f / _speedCellPerSec)
        {
            _currentTravelTime += Time.deltaTime;
            return;
        }

        var moveVector = new Vector2(Mathf.Round(hAxis), Mathf.Round(vAxis));
        moveVector = CheckExceedBoundary(moveVector);

        if (!isInvincible) GameController.Instance.MoveObject(this, moveVector);
        else SetPosition(CurrentPosition += moveVector); 
        
        _currentTravelTime = 0f;
    }

    private Vector2 CheckExceedBoundary(Vector3 moveVector)
    {
        var playArea = GameController.Instance.PlayAreaSize;
        if (!isLimitPlayArea) return moveVector;
        
        if (CurrentPosition.y + moveVector.y > playArea.y * _playSpaceInPercent) moveVector.y = 0;
        if (CurrentPosition.y + moveVector.y < 0) moveVector.y = 0;
        if (CurrentPosition.x + moveVector.x >= playArea.x) moveVector.x = 0;
        if (CurrentPosition.x + moveVector.x < 0) moveVector.x = 0;
        return moveVector;
    }

    public override void Collide(BlockObject targetObject)
    {
        base.Collide(targetObject);
        if (targetObject is CentipedeHeadObject || targetObject is CentipedeBodyObject)
        {
            HitCentipede();
        }
    }

    public void HitCentipede()
    {
        _PlayerController.PlayerHit();
    }

    public void PlayInvincibleEffect()
    {
        _isInvincible = true;
        InvincibleEffect();
    }

    public void StopInvincibleEffect()
    {
        _isInvincible = false;
    }
    
    private async void InvincibleEffect()
    {
        bool fadeOut = true;
        while (_isInvincible)
        {
            var color = playerGraphic.color;
            color.a += fadeOut ? -Time.deltaTime * 3f : + Time.deltaTime * 3f;
            if (color.a >= 1)
            {
                color.a = 1;
                fadeOut = !fadeOut;
            }else if (color.a <= 0)
            {
                color.a = 0;
                fadeOut = !fadeOut;
            }
            playerGraphic.color = color;
            await Task.Yield();
        }
        
        var playerColor = playerGraphic.color;
        playerColor.a = 1;
        playerGraphic.color = playerColor;
    }
}
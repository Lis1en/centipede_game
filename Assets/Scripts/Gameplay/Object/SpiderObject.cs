using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class SpiderObject : BlockObject
{
    private SpiderController _spiderController;
    private float _speedCellPerSec;
    private float _currentTravelTime;
    private float _spiderHoldTime;

    private float _holdTimeCount;
    
    public void SetConfig(SpiderController spiderController, float speedCellPerSec, float spiderHoldTime)
    {
        _spiderController = spiderController;
        _speedCellPerSec = speedCellPerSec;
        _spiderHoldTime = spiderHoldTime;
    }

    private void Update()
    {
        if (GameController.Instance.CurrentState != GameState.Playing) return;
        
        MoveRoutine();
        HoldCheck();
    }

    private void HoldCheck()
    {
        var targetPos = GetTargetPosition(GameController.Instance.PlayerPos);
        if (targetPos != CurrentPosition)
        {
            _holdTimeCount = 0;
            return;
        }
        
        _holdTimeCount += Time.deltaTime;
        if (_holdTimeCount >= _spiderHoldTime)
        {
            GameController.Instance.PlayerKill();
        }
        
    }

    private void MoveRoutine()
    {
        if (_currentTravelTime < 1f / _speedCellPerSec)
        {
            _currentTravelTime += Time.deltaTime;
            return;
        }

        Move();
        _currentTravelTime = 0f;
    }

    private Vector2 GetTargetPosition(Vector2 playerPos)
    {
        playerPos.y += 1;
        return playerPos;
    }

    private void Move()
    {
        // Find path
        var startPos = CurrentPosition;
        var playerPos = GameController.Instance.PlayerPos;
        var targetPos = GetTargetPosition(playerPos);

        // Check target is empty to go to otherwise spider will go to adjacent position
        var isTargetPosValid = IsValidTarget(targetPos);
        var diffCount = 1;
        var switcher = 1;
        while (!isTargetPosValid)
        {
            targetPos.x = playerPos.x;
            targetPos.x += diffCount;
            isTargetPosValid = IsValidTarget(targetPos);
            if (switcher == -1) diffCount = Mathf.Abs(diffCount) + 1;
            switcher *= -1;
            diffCount *= switcher;
        }
        
        targetPos = PathFinderController.FindPath(startPos, targetPos);
        GameController.Instance.PlaceObject(this, targetPos);
    }

    private bool IsValidTarget(Vector2 targetPos)
    {
        var playArea = GameController.Instance.PlayAreaSize;
        if (targetPos.x < 0 || targetPos.x >= playArea.x || targetPos.y < 0 || targetPos.y >= playArea.y) return false;
            
        var targetBlock = GameController.Instance.GetBoardTarget((int) targetPos.x, (int) targetPos.y);
        return targetBlock == null || targetBlock == this;
    }

    public override void DeSpawn()
    {
        _spiderController.RemoveSpider(this);
        GameController.Instance.RemoveObject(this);
        SpiderObjectPool.Instance.ReturnObject(transform);
        base.DeSpawn();
    }

    public override void Collide(BlockObject targetObject)
    {
        base.Collide(targetObject);
        if (!(targetObject is BulletObject)) return;
        
        GameController.Instance.DestroySpider();
        targetObject.DeSpawn();
        DeSpawn();
    }
}
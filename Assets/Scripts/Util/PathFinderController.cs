using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PathFinderController
{
    private static int _playAreaSizeX;
    private static int _playAreaSizeY;

    public static void Init()
    {
        var playArea = GameController.Instance.PlayAreaSize;
        _playAreaSizeX = (int) playArea.x;
        _playAreaSizeY = (int) playArea.y;
    }

    public static Vector2 FindPath(Vector2 startPos, Vector2 targetPos)
    {
        List<Node> evaluated = new List<Node>();
        List<Node> notEvaluate = new List<Node>();

        Node startNode = new Node((int) startPos.x, (int) startPos.y);
        notEvaluate.Add(startNode);
        Node currentNode;
        int count = 0;
        while (true)
        {
            notEvaluate = notEvaluate.OrderBy(node => node.FCost).ThenBy(node => node.HCost).ToList();
            currentNode = notEvaluate.First();
            notEvaluate.Remove(currentNode);

            if (currentNode.NodePos == targetPos) break;

            var neighborNode = GetNeighborNode(currentNode);
            foreach (var node in neighborNode)
            {
                if (evaluated.Any(targetNode => targetNode.NodePos == node.NodePos)) continue;

                var gCost = currentNode.GCost + GetDistance(currentNode.NodePos, node.NodePos);
                var hCost = GetDistance(targetPos, node.NodePos);
                node.SetCost(gCost, hCost, currentNode);

                var index = notEvaluate.FindIndex(targetNode => targetNode.NodePos == node.NodePos);
                if (index != -1)
                {
                    if (notEvaluate[index].FCost < node.FCost || (notEvaluate[index].FCost == node.FCost && node.HCost < notEvaluate[index].HCost)) continue;
                    notEvaluate[index] = node;
                }
                else
                {
                    notEvaluate.Add(node);
                }
            }

            evaluated.Add(currentNode);
            
            count++;
            if (count > 1000) break;
        }

        var path = TracePath(currentNode);
        // Path finding log
        // string pathStr = "";
        // path.ForEach(item => pathStr += $" | {item} ");
        // Debug.Log(pathStr);
        return path.Count == 0 ? targetPos : path.First();
    }

    private static int GetDistance(Vector2 aPos, Vector2 bPos)
    {
        var xDist = Mathf.Abs((int) aPos.x - (int) bPos.x);
        var yDist = Mathf.Abs((int) aPos.y - (int) bPos.y);

        if (xDist > yDist) return yDist * 14 + xDist * 10;
        if (yDist > xDist) return xDist * 14 + yDist * 10;
        return xDist * 14;
    }

    private static List<Node> GetNeighborNode(Node currentNode)
    {
        var neighborList = new List<Node>();
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0) continue;

                var targetX = currentNode.X + x;
                var targetY = currentNode.Y + y;

                bool isNotOnGrid = targetX < 0 || targetX >= _playAreaSizeX || targetY < 0 || targetY >= _playAreaSizeY;
                if (isNotOnGrid) continue;

                bool isTargetBlocked = GameController.Instance.GetBoardTarget(targetX, targetY) != null;
                if (isTargetBlocked) continue;

                neighborList.Add(new Node(targetX, targetY));
            }
        }

        return neighborList;
    }

    private static List<Vector2> TracePath(Node node)
    {
        List<Vector2> path = new List<Vector2>();
        while (true)
        {
            if (node.ParentNode == null) break;
            path.Add(node.NodePos);
            node = node.ParentNode;
        }

        path.Reverse();
        return path;
    }
}


public class Node
{
    public int X;
    public int Y;
    public Vector2 NodePos => new Vector2(X, Y);
    public int GCost;
    public int HCost;
    public int FCost => GCost + HCost;
    public Node ParentNode;

    public Node(int x, int y)
    {
        X = x;
        Y = y;
    }

    public void SetCost(int gCost, int hCost, Node parentNode)
    {
        GCost = gCost;
        HCost = hCost;
        ParentNode = parentNode;
    }
}
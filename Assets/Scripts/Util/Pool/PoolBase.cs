using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public abstract class PoolBase<T> : Singleton<PoolBase<T>>
{
    [SerializeField] private Transform poolContainer;
    [SerializeField] private GameObject originPrefab;
    [SerializeField] private int poolSize;

    private Stack<T> _currentPool;

    protected override void Awake()
    {
        base.Awake();
        GeneratePool();
    }

    private void GeneratePool()
    {
        _currentPool = new Stack<T>();
        for (int i = 0; i < poolSize; i++) _currentPool.Push(SpawnObject());
    }

    private T SpawnObject()
    {
        return Instantiate(originPrefab, poolContainer).GetComponent<T>();
    }

    public T GetObject()
    {
        return _currentPool.Count <= 0 ? SpawnObject() : _currentPool.Pop();
    }

    public void ReturnObject(Transform poolObject)
    {
        T poolComponent = poolObject.GetComponent<T>();
        poolObject.transform.SetParent(poolContainer);
        _currentPool.Push(poolComponent);
    }
}
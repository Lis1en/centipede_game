using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public static class TypeConvertor
{
    public static Direction Vector2ToDirection(Vector2 vector)
    {
        if (vector.normalized == Vector2.right || math.abs(vector.normalized.x) > math.abs(vector.normalized.y) && vector.normalized.x > 0) return Direction.Right;
        if (vector.normalized == Vector2.left || math.abs(vector.normalized.x) > math.abs(vector.normalized.y) && vector.normalized.x < 0) return Direction.Left;
        if (vector.normalized == Vector2.down || math.abs(vector.normalized.y) > math.abs(vector.normalized.x) && vector.normalized.y < 0) return Direction.Down;
        if (vector.normalized == Vector2.up || math.abs(vector.normalized.y) > math.abs(vector.normalized.x) && vector.normalized.y > 0) return Direction.Up;
        return Direction.None;
    }
}

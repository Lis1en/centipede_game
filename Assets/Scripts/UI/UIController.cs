using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : Singleton<UIController> 
{
    [Header("StartGame")]
    [SerializeField] private GameObject startGameContainer;
    [Header("GamePlay")]
    [SerializeField] private GameObject gameplayContainer;
    [SerializeField] private Text playerLifeText;
    [SerializeField] private Text scoreText;
    [Header("GameOver")]
    [SerializeField] private GameObject gameOverContainer;
    [SerializeField] private Text finialScoreText;
    
    public void StartGame()
    {
        startGameContainer.SetActive(false);
        gameplayContainer.SetActive(true);
    }

    public void UpdatePlayerLife(int life)
    {
        playerLifeText.text = $"Life : {life}";
    }

    public void UpdateScore(int score)
    {
        scoreText.text = $"Score : {score}";
    }

    public void GameOver(int score)
    {
        finialScoreText.gameObject.SetActive(score > 0);
        finialScoreText.text = $"SCORE : {score}";
        gameplayContainer.SetActive(false);
        gameOverContainer.SetActive(true);
    }

    public void ReStartGame()
    {
        gameOverContainer.SetActive(false);
        startGameContainer.SetActive(true);
    }
}

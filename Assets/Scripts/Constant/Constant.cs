
public static class Constant
{
    public static class PlayerInput
    {
        public const string Horizontal = "Horizontal";
        public const string Vertical = "Vertical";
    }

    public static class CentipedePart
    {
        public const int HeadIndex = -1;
    }
}

public enum Direction
{
    None = -1,
    Right = 0 ,
    Down = 1,
    Left = 2,
    Up = 3
}

public enum GameState
{
    Start,
    Playing,
    Over,
    Reset,
}
